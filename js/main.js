// THEORY

// 1. DOM структура документа - структура у вигляді дерева, яка перетворює HTML код в JS код і дозволяє змінювати HTML елементи   

// 2. innerHTML повертає всі дочірні елементи всередині тега, а innerText - повертає текстову внутрянку дочірніх елементів

// 3. Існує декілька способів звернутися до елемента: getElementById, getElementsByClassName, getElementsByTagName, querySelector,
// querySelectorAll, квері селектрок складніший - тому працює трохи повільніше, краще використовувати методи для пошуку живих колекцій

// TASK 1
let paragraphs = document.body.getElementsByTagName('p')

for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000'
}

// TASK 2
console.log("TASK 2")
console.log(document.body.querySelector('#optionsList'))
console.log(document.body.querySelector('#optionsList').parentElement)
console.log(document.body.querySelector('#optionsList').childNodes)
let d = document.body.querySelector('#optionsList').childNodes
for (let f = 0; f < d.length; f++) {
    console.log(`${d[f].nodeName} ${d[f].nodeType}`)
}
// TASK 3

let a = document.body.querySelector('#testParagraph');
a.textContent = "This is a paragraph";

// TASK 4
console.log("TASK 4")
let b = document.body.querySelector('.main-header');
console.log(b.children)

for (let j = 0; j < b.children.length; j++) {
    b.children[j].classList.add('nav-item')
    console.log(b.children[j])
}

// document.body.querySelector('.main-header').children[0].classList.add('nav-item')
// console.log(document.body.querySelector('.main-header').children[0])
// document.body.querySelector('.main-header').children[1].classList.add('nav-item')
// console.log(document.body.querySelector('.main-header').children[1])

// TASK 5
console.log("TASK 5")
let c = document.body.querySelectorAll('.section-title');
console.log(c)

for (let k = 0; k < c.length; k++) {
    c[k].classList.remove('.section-title')
    console.log(b.children[k])
}